<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetFile;
use App\Http\Resources\AssetCollection;
use App\Http\Resources\AssetResource;
use Illuminate\Support\Carbon;
use Storage;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new AssetCollection(Asset::orderBy('start_at', 'ASC')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:assets',
            'type' => 'required',
            'url' => 'required_if:type,Webpage',
            'active' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
            'duration' => 'required',
            'media' => 'required_if:type,Media'
        ]);

        if ($request->type != 'Media' && $request->type != 'Webpage') {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'type' => 'Type must be Media or Webpage.'
                ]
            ], 422);
        }

        if ((new Carbon($request->start_at)) > (new Carbon($request->end_at))) {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'start_at' => 'Must be younger than end_at.'
                ]
            ], 422);
        }

        $asset = Asset::create($request->all());

        
        if ($request->has('media')) {
            $assetFile = AssetFile::create([
                'path' => asset(Storage::url($request->file('media')->store('public/' . preg_replace('#[ -]+#', '-', $request->name)))),
                'asset_id' => $asset->id,
                'type' => $request->file('media')->getMimeType()
            ]);
        }

        return response()->json(['message' => 'Asset Created', 'data' => (new AssetResource($asset))], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        return response()->json(new AssetResource($asset));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'path' => 'required_if:type,Video',
            'url' => 'required_if:type,Webpage',
            'active' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
            'duration' => 'required'
        ]);

        $asset->update($request->all());

        return response()->json($asset, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $asset->delete();

        return response()->json(['message' => 'Asset Sucessfully Deleted'], 200);
    }
}
