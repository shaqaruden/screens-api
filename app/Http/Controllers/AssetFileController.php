<?php

namespace App\Http\Controllers;

use App\AssetFile;
use Illuminate\Http\Request;
use Storage;

class AssetFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Asset $asset)
    {
        return response()->json($asset->files, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json([ 'message' => asset(Storage::url($request->file('media')->store('public/' . $request->asset_name)))], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetFile  $assetFile
     * @return \Illuminate\Http\Response
     */
    public function show(AssetFile $assetFile)
    {
        return resposne()->json($assetFile, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssetFile  $assetFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetFile $assetFile)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetFile  $assetFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetFile $assetFile)
    {
        $assetFile->delete();

        return response()->json([ 'message' => 'File Deleted'], 200);
    }
}
