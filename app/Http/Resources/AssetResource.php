<?php

namespace App\Http\Resources;
 
use Illuminate\Http\Resources\Json\JsonResource;

class AssetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'properties' => [
                'active' => $this->active,
                'url' => $this->url,
                'path' => $this->path,
                'start_at' => $this->start_at,
                'end_at' => $this->end_at,
                'duration' => $this->duration
            ],
            'relationships' => [
                'asset' => $this->file
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
