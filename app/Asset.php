<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $fillable = ['name', 'type', 'active', 'url', 'start_at', 'end_at', 'duration'];

    public function file() {
        return $this->hasOne(AssetFile::class, 'asset_id');
    }
}
