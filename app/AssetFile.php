<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetFile extends Model
{
    protected $fillable = ['path', 'asset_id', 'type'];

    public function asset() {
        return $this->belongsTo(Asset::class);
    }
}
